package org.bytestruct.fantasytools.app;

import javax.annotation.Nonnull;

public class Launcher {
	public static void main (@Nonnull String[] args) {
		FantasyApp.main(args);
	}
}
