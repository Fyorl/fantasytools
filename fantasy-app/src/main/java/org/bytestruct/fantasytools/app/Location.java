package org.bytestruct.fantasytools.app;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;

class Location {
	@Nonnull private final String name;
	@Nonnull private final List<Location> subLocations = new ArrayList<>();

	Location (@Nonnull String name) {
		this.name = name;
	}

	@Nonnull
	String getName () {
		return name;
	}

	@Nonnull
	List<Location> getSubLocations () {
		return subLocations;
	}
}
