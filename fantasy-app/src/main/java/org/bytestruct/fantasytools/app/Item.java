package org.bytestruct.fantasytools.app;

import org.bytestruct.fantasytools.data.Datum;
import org.json.JSONObject;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

class Item implements Comparable<Item> {
	@Nonnull private final String name;
	@Nonnull private final String type;
	@Nonnull private final Map<String, String> soldBy = new HashMap<>();
	@Nonnull private final Map<String, String> droppedBy = new HashMap<>();

	private Item (
		@Nonnull String name,
		@Nonnull String type,
		@Nonnull Map<String, String> soldBy,
		@Nonnull Map<String, String> droppedBy)
	{
		this.name = name;
		this.type = type;
		this.soldBy.putAll(soldBy);
		this.droppedBy.putAll(droppedBy);
	}

	@Nonnull
	static Item fromRow (@Nonnull Map<String, Datum> row) {
		final var name = row.get("name").get();
		final var type = row.get("type").get();
		final var sellers = row.get("sellers").get();
		final var droppedBy = row.get("dropped_by").get();
		final Map<String, String> sellersMap;
		final Map<String, String> droppedByMap;

		assert name != null;
		assert type != null;

		if (sellers == null) {
			sellersMap = Collections.emptyMap();
		} else {
			sellersMap = stringMapOf(new JSONObject(sellers));
		}

		if (droppedBy == null) {
			droppedByMap = Collections.emptyMap();
		} else {
			droppedByMap = stringMapOf(new JSONObject(droppedBy));
		}

		return new Item(name, type, sellersMap, droppedByMap);
	}

	@Nonnull
	private static Map<String, String> stringMapOf (@Nonnull JSONObject json) {
		final var map = new HashMap<String, String>();
		final var keys = json.keys();

		while (keys.hasNext()) {
			final var key = keys.next();
			final var val = json.get(key);

			if (val instanceof String) {
				map.put(key, (String) val);
			}
		}

		return map;
	}

	@Nonnull
	String getName () {
		return name;
	}

	@Nonnull
	String getType () {
		return type;
	}

	@Nonnull
	Map<String, String> getSoldBy () {
		return soldBy;
	}

	@Nonnull
	Map<String, String> getDroppedBy () {
		return droppedBy;
	}

	@Override
	public int compareTo (Item o) {
		return name.compareTo(o.getName());
	}
}
