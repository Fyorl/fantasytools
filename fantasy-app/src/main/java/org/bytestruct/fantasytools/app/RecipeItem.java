package org.bytestruct.fantasytools.app;

import javax.annotation.Nonnull;

class RecipeItem {
	@Nonnull private final String item;
	private final int amount;

	RecipeItem (@Nonnull String item, int amount) {
		this.item = item;
		this.amount = amount;
	}

	@Nonnull
	String getItem () {
		return item;
	}

	int getAmount () {
		return amount;
	}
}
