package org.bytestruct.fantasytools.app;

import org.bytestruct.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.annotation.Nonnull;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static org.bytestruct.fantasytools.data.Data.fetch;

class Model {
	@Nonnull private static final Logger logger = Logger.getLogger(Model.class);
	@Nonnull private final Map<String, Item> items = new HashMap<>();
	@Nonnull private final Map<Item, Map<String, List<Location>>> locations = new HashMap<>();
	@Nonnull private final Map<Item, Map<String, List<RecipeItem>>> recipes = new HashMap<>();
	private final boolean loadError;

	Model (@Nonnull Path db) {
		try (var conn = DriverManager.getConnection(jdbcURL(db))) {
			loadItems(conn);
			loadLocations(conn);
			loadRecipes(conn);
		} catch (SQLException e) {
			logger.error("Failed to read database.", e);
			loadError = true;
			return;
		}

		loadError = false;
	}

	boolean isLoadError () {
		return loadError;
	}

	@Nonnull
	Map<String, Item> getItems () {
		return items;
	}

	@Nonnull
	Map<Item, Map<String, List<Location>>> getLocations () {
		return locations;
	}

	@Nonnull
	Map<Item, Map<String, List<RecipeItem>>> getRecipes () {
		return recipes;
	}

	@Nonnull
	private List<Location> constructLocations (@Nonnull JSONObject json) {
		final var locations = new ArrayList<Location>();
		final var keys = json.keys();

		while (keys.hasNext()) {
			final var key = keys.next();
			final var location = new Location(key);
			locations.add(location);

			final var child = json.get(key);
			if (child instanceof JSONObject) {
				location.getSubLocations().addAll(constructLocations((JSONObject) child));
			} else if (child instanceof JSONArray) {
				location.getSubLocations().addAll(constructLocations((JSONArray) child));
			}
		}

		return locations;
	}

	private List<Location> constructLocations (@Nonnull JSONArray json) {
		final var locations = new ArrayList<Location>();
		for (var i = 0; i < json.length(); i++) {
			locations.add(new Location(json.getString(i).replaceFirst("^\\d+=", "")));
		}

		return locations;
	}

	private void loadItems (@Nonnull Connection conn) throws SQLException {
		try (var stmt = conn.createStatement();
			var rs = stmt.executeQuery("SELECT * FROM Items"))
		{
			items.putAll(
				fetch(rs).parallelStream()
					.map(Item::fromRow)
					.collect(Collectors.toMap(
						Item::getName,
						Function.identity())));
		}
	}

	private void loadLocations (@Nonnull Connection conn) throws SQLException {
		try (var stmt = conn.createStatement();
			var rs = stmt.executeQuery(
				"SELECT name, Locations.* FROM Items, Locations "
				+ "WHERE Items.item_id = Locations.item_fk"))
		{
			fetch(rs).forEach(row -> {
				final var item = items.get(row.get("name").get());
				final var professionMap = locations.computeIfAbsent(item, x -> new HashMap<>());
				final var locationList =
					professionMap.computeIfAbsent(
						row.get("profession").get(),
						x -> new ArrayList<>());

				final var locationJSON = row.get("location").get();
				if (locationJSON != null) {
					locationList.addAll(constructLocations(new JSONObject(locationJSON)));
				}
			});
		}
	}

	private void loadRecipes (@Nonnull Connection conn) throws SQLException {
		try (var stmt = conn.createStatement();
			var rs = stmt.executeQuery(
				"SELECT name, Recipes.* FROM Items, Recipes WHERE Items.item_id = Recipes.item_fk"))
		{
			fetch(rs).forEach(row -> {
				final var item = items.get(row.get("name").get());
				final var professionMap = recipes.computeIfAbsent(item, x -> new HashMap<>());
				final var recipeList =
					professionMap.computeIfAbsent(
						row.get("profession").get(),
						x -> new ArrayList<>());

				final var recipeJSON = row.get("recipe").get();
				if (recipeJSON != null) {
					final var json = new JSONObject(recipeJSON);
					final var keys = json.keys();
					while (keys.hasNext()) {
						final var key = keys.next();
						recipeList.add(new RecipeItem(key, json.getInt(key)));
					}
				}
			});
		}
	}

	@Nonnull
	private static String jdbcURL (@Nonnull Path db) {
		return format("jdbc:sqlite:%s", db);
	}
}
