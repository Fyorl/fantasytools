package org.bytestruct.fantasytools.app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import org.bytestruct.logging.Logger;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FantasyApp extends Application {
	@Nonnull private static final Logger logger = Logger.getLogger(FantasyApp.class);

	static void main (@Nonnull String[] args) {
		launch(args);
	}

	@Override
	public void start (@Nonnull Stage stage) {
		final var params = getParameters().getNamed();
		final Path db;

		if (params.containsKey("db")) {
			db = Paths.get(params.get("db"));
		} else {
			db = Paths.get("spider.db");
		}

		final var model = new Model(db);
		if (model.isLoadError()) {
			final var alert =
				new Alert(
					AlertType.ERROR,
					"Failed to open database. Please check log for details.");
			alert.showAndWait();
			System.exit(1);
		}

		try {
			final var loader = new FXMLLoader(getClass().getResource("/main.fxml"));
			final Parent root = loader.load();
			final var scene = new Scene(root);
			final MainController controller = loader.getController();

			controller.setDebugMode(
				params.containsKey("debug") && Boolean.parseBoolean(params.get("debug")));
			controller.loadData(model);
			stage.setTitle("Fantasy Tools");
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			logger.error("Failed to load FXML.", e);
		}
	}
}
