package org.bytestruct.fantasytools.app;

import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Worker.State;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import org.bytestruct.logging.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static java.nio.charset.StandardCharsets.UTF_8;

public class MainController {
	@Nonnull private static final Logger logger = Logger.getLogger(MainController.class);
	@Nonnull private static final String MASTER_CRAFT_ID = "master-craft-list";
	@Nullable private Model model = null;
	@FXML private TextField search;
	@FXML private VBox listContainer;
	@FXML public WebView viewer;
	private boolean debugMode;

	void loadData (@Nonnull Model model) {
		this.model = model;
		updateList(FXCollections.observableArrayList(model.getItems().values()).sorted());
		search.setOnKeyReleased(this::filterList);
		viewer.getEngine().setJavaScriptEnabled(true);
		viewer.getEngine().load(getClass().getResource("/viewer.html").toExternalForm());
		viewer.setOnKeyPressed(this::handleKeyPress);
		viewer.getEngine().locationProperty().addListener(this::handleItemClick);
		viewer.getEngine().setOnError(error ->
			logger.error("Javascript error: %s", error.getException(), error.getMessage()));
		viewer.getEngine().setOnAlert(evt ->
			new Alert(AlertType.INFORMATION, evt.getData()).showAndWait());
		viewer.getEngine().getLoadWorker().exceptionProperty().addListener(
			(changed, oldValue, newValue) -> logger.error("Javascript error:", newValue));
		viewer.getEngine().getLoadWorker().stateProperty().addListener(
			(changed, oldValue, newValue) -> {
				if (newValue == State.SUCCEEDED) {
					viewer.getEngine().executeScript(format("setDebugMode(%b);", debugMode));
				}
			});
	}

	void setDebugMode (boolean debugMode) {
		this.debugMode = debugMode;
	}

	private void constructCraftTree (
		@Nonnull Document document,
		@Nonnull Element rootContainer,
		@Nonnull List<Item> craftList,
		@Nonnull TObjectIntMap<String> aggregate)
	{
		assert model != null;
		final var rootList = document.createElement("ul");
		rootContainer.appendChild(rootList);

		final var recipeItems =
			craftList.stream()
				.map(item -> new RecipeItem(item.getName(), 1))
				.collect(Collectors.toList());

		constructCraftTree(document, aggregate, recipeItems, rootList, 1, true);
	}

	private void constructCraftTree (
		@Nonnull Document document,
		@Nonnull TObjectIntMap<String> aggregate,
		@Nonnull List<RecipeItem> recipe,
		@Nonnull Element container,
		int multiplier,
		boolean root)
	{
		assert model != null;
		for (var recipeItem : recipe) {
			final var totalAmount = multiplier * recipeItem.getAmount();
			final var mainListItem = document.createElement("li");
			container.appendChild(mainListItem);

			if (root) {
				final var itemName = document.createElement("strong");
				itemName.setTextContent(recipeItem.getItem());
				mainListItem.appendChild(itemName);
			} else {
				final var link = document.createElement("a");
				final var amountNode = document.createTextNode(format("%dx ", totalAmount));
				link.setAttribute("href", "#" + URLEncoder.encode(recipeItem.getItem(), UTF_8));
				link.setTextContent(recipeItem.getItem());
				mainListItem.appendChild(amountNode);
				mainListItem.appendChild(link);
			}

			final var item = model.getItems().get(recipeItem.getItem());
			final var subRecipes = model.getRecipes().get(item);

			if (subRecipes == null) {
				aggregate.adjustOrPutValue(item.getName(), totalAmount, totalAmount);
				continue;
			}

			final var subRecipe = subRecipes.values().iterator().next();
			final var subListItem = document.createElement("li");
			final var subList = document.createElement("ul");

			subListItem.appendChild(subList);
			container.appendChild(subListItem);
			constructCraftTree(
				document,
				aggregate,
				subRecipe,
				subList,
				totalAmount,
				false);
		}
	}

	private void createInfoBox (@Nonnull MouseEvent event, @Nonnull Item item) {
		if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
			createInfoBox(item);
		}
	}

	private void createInfoBox (@Nonnull Item item) {
		assert model != null;
		final var document = viewer.getEngine().getDocument();
		final var body = document.getElementsByTagName("body").item(0);

		if (infoBoxAlreadyAdded(body, item.getName())) {
			return;
		}

		final var infoBox = document.createElement("div");
		final var header = document.createElement("div");
		final var title = document.createElement("div");
		final var type = document.createElement("div");
		infoBox.setAttribute("class", "info-box");
		infoBox.setAttribute("data-id", item.getName());
		header.setAttribute("class", "section");
		title.setAttribute("class", "title");
		type.setAttribute("class", "type");
		title.setTextContent(item.getName());
		type.setTextContent(item.getType());
		header.appendChild(title);
		header.appendChild(type);
		infoBox.appendChild(header);
		addBasicInfo(document, infoBox, item.getSoldBy());
		addBasicInfo(document, infoBox, item.getDroppedBy());
		addLocations(document, infoBox, model.getLocations().get(item));
		addRecipes(document, infoBox, model.getRecipes().get(item));
		body.appendChild(infoBox);
	}

	private void deleteSelected () {
		getSelected().forEach(element -> element.getParentNode().removeChild(element));
		refreshMasterList();
		viewer.getEngine().executeScript("pckry.layout()");
	}

	@SuppressWarnings("unused")
	private void filterList (@Nonnull KeyEvent event) {
		assert model != null;
		final var needle = search.getText();
		final ObservableList<Item> matches =
			model.getItems().entrySet().parallelStream()
				.filter(e -> e.getKey().toLowerCase().contains(needle))
				.map(Entry::getValue)
				.collect(Collectors.toCollection(FXCollections::observableArrayList))
				.sorted();

		updateList(matches);
	}

	@Nonnull
	private List<Element> getSelected () {
		final var document = viewer.getEngine().getDocument();
		final var nodeList = document.getElementsByTagName("div");
		final var elements = new ArrayList<Element>();

		for (var i = 0; i < nodeList.getLength(); i++) {
			if (nodeList.item(i) instanceof Element) {
				final var element = (Element) nodeList.item(i);
				if (hasClass(element, "selected")) {
					elements.add(element);
				}
			}
		}

		return elements;
	}

	@SuppressWarnings("unused")
	private void handleItemClick (
		@Nonnull ObservableValue<? extends String> changed,
		@Nonnull String oldValue,
		@Nonnull String newValue)
	{
		assert model != null;
		final var hashIndex = newValue.lastIndexOf("#");
		if (hashIndex < 0 || hashIndex + 1 >= newValue.length()) {
			return;
		}

		final var hash = URLDecoder.decode(newValue.substring(hashIndex + 1), UTF_8);
		final var item = model.getItems().get(hash);
		createInfoBox(item);
	}

	private void handleKeyPress (@Nonnull KeyEvent event) {
		switch (event.getCode()) {
			case DELETE: deleteSelected(); break;
			case ENTER: markSelectedForCrafting(); break;
		}
	}

	private void markSelectedForCrafting () {
		final var selected = getSelected();
		final var allCrafting =
			selected.stream()
				.filter(element -> hasClass(element, "crafting"))
				.count() == selected.size();

		if (allCrafting) {
			selected.forEach(element -> removeClass(element, "crafting"));
		} else {
			selected.forEach(element -> addClass(element, "crafting"));
		}

		selected.forEach(element -> removeClass(element, "selected"));
		refreshMasterList();
		viewer.getEngine().executeScript("pckry.layout()");
	}

	private void refreshMasterList () {
		assert model != null;
		final var document = viewer.getEngine().getDocument();
		final var masterElement = document.getElementById(MASTER_CRAFT_ID);
		final var sections = masterElement.getElementsByTagName("div");
		final var borderSections = new ArrayList<Node>();

		for (var i = 0; i < sections.getLength(); i++) {
			final var section = sections.item(i);
			if (section instanceof Element) {
				if (hasClass((Element) section, "bordered")) {
					borderSections.add(section);
				}
			}
		}

		borderSections.forEach(masterElement::removeChild);
		final var divs = document.getElementsByTagName("div");
		final var craftingList = new ArrayList<Item>();

		for (var i = 0; i < divs.getLength(); i++) {
			if (!(divs.item(i) instanceof Element)) {
				continue;
			}

			final var div = (Element) divs.item(i);
			if (hasClass(div, "crafting")) {
				final var itemID = div.getAttribute("data-id");
				final var item = model.getItems().get(itemID);

				if (item != null) {
					craftingList.add(item);
				}
			}
		}

		if (craftingList.size() < 1) {
			return;
		}

		final var craftTreeSection = document.createElement("div");
		final var aggregateSection = document.createElement("div");
		craftTreeSection.setAttribute("class", "section bordered");
		aggregateSection.setAttribute("class", "section bordered");
		masterElement.appendChild(craftTreeSection);
		masterElement.appendChild(aggregateSection);

		final var aggregate = new TObjectIntHashMap<String>();
		constructCraftTree(document, craftTreeSection, craftingList, aggregate);
		writeAggregate(document, aggregateSection, aggregate);
	}

	private void updateList (@Nonnull List<Item> items) {
		final var list = new ListView<Item>();
		list.setCellFactory(x -> new ItemCell());
		list.getItems().addAll(items);
		VBox.setVgrow(list, Priority.ALWAYS);
		listContainer.getChildren().clear();
		listContainer.getChildren().add(list);
	}

	private class ItemCell extends ListCell<Item> {
		private ItemCell () {
			super();
			setOnMouseClicked(event -> createInfoBox(event, getItem()));
		}

		@Override
		protected void updateItem (@Nullable Item item, boolean empty) {
			super.updateItem(item, empty);
			if (item != null) {
				setText(format("%s (%s)", item.getName(), item.getType()));
			}
		}
	}

	private static void addBasicInfo (
		@Nonnull Document document,
		@Nonnull Element container,
		@Nonnull Map<String, String> info)
	{
		if (info.size() < 1) {
			return;
		}

		final var section = document.createElement("div");
		section.setAttribute("class", "section bordered");
		container.appendChild(section);
		info.forEach((key, val) -> {
			final var row = document.createElement("div");
			final var keyElement = document.createElement("strong");
			final var valElement = document.createTextNode(format(" %s", display(val)));
			keyElement.setTextContent(key);
			row.appendChild(keyElement);
			row.appendChild(valElement);
			section.appendChild(row);
		});
	}

	@SuppressWarnings("SameParameterValue")
	private static void addClass (@Nonnull Element element, @Nonnull String cls) {
		final var className = element.getAttribute("class");
		if (className == null || className.length() < 1) {
			element.setAttribute("class", cls);
			return;
		}

		final var classes = Set.of(className.split(" "));
		if (!classes.contains(cls)) {
			element.setAttribute("class", format("%s %s", className, cls));
		}
	}

	private static void addLocations (
		@Nonnull Document document,
		@Nonnull Element container,
		@Nullable Map<String, List<Location>> professionMap)
	{
		if (professionMap == null) {
			return;
		}

		final var section = document.createElement("div");
		section.setAttribute("class", "section bordered");
		container.appendChild(section);
		professionMap.forEach((profession, locations) -> {
			final var ul = document.createElement("ul");
			final var heading = document.createElement("li");
			final var item = document.createElement("strong");
			item.setTextContent(profession);
			heading.appendChild(item);
			ul.appendChild(heading);
			section.appendChild(ul);
			locations.stream()
				.map(location -> deconstructLocation(document, location))
				.forEach(ul::appendChild);
		});

	}

	private static void addRecipes (
		@Nonnull Document document,
		@Nonnull Element container,
		@Nullable Map<String, List<RecipeItem>> professionMap)
	{
		if (professionMap == null) {
			return;
		}

		final var section = document.createElement("div");
		section.setAttribute("class", "section bordered");
		container.appendChild(section);
		professionMap.forEach((profession, recipeItems) -> {
			final var ul = document.createElement("ul");
			final var heading = document.createElement("li");
			final var recipe = document.createElement("li");
			final var headingItem = document.createElement("strong");
			final var recipeList = document.createElement("ul");
			headingItem.setTextContent(profession);
			heading.appendChild(headingItem);
			recipe.appendChild(recipeList);
			ul.appendChild(heading);
			ul.appendChild(recipe);
			section.appendChild(ul);
			recipeItems.stream().map(recipeItem -> {
				final var listItem = document.createElement("li");
				final var amount = document.createTextNode(format("%dx ", recipeItem.getAmount()));
				final var item = document.createElement("strong");
				item.setTextContent(recipeItem.getItem());
				listItem.appendChild(amount);
				listItem.appendChild(item);
				return listItem;
			}).forEach(recipeList::appendChild);
		});
	}

	@Nonnull
	private static Element deconstructLocation (
		@Nonnull Document document,
		@Nonnull Location location)
	{
		final var li = document.createElement("li");
		if (location.getSubLocations().size() < 1) {
			li.setTextContent(location.getName());
		} else {
			final var ul = document.createElement("ul");
			final var heading = document.createElement("li");
			final var name = document.createElement("strong");
			name.setTextContent(location.getName());
			heading.appendChild(name);
			ul.appendChild(heading);
			li.appendChild(ul);
			location.getSubLocations().stream()
				.map(subLocation -> deconstructLocation(document, subLocation))
				.forEach(ul::appendChild);
		}

		return li;
	}

	@Nonnull
	private static String display (@Nonnull String s) {
		return s.replace("Other Locations", "").trim();
	}

	private static boolean hasClass (@Nonnull Element element, @Nonnull String cls) {
		final var className = element.getAttribute("class");
		return className != null && Set.of(className.split(" ")).contains(cls);
	}

	private static boolean infoBoxAlreadyAdded (@Nonnull Node body, @Nonnull String id) {
		final var boxes = body.getChildNodes();
		for (var i = 0; i < boxes.getLength(); i++) {
			final var infoBox = boxes.item(i);
			if (infoBox instanceof Element
				&& id.equals(((Element) infoBox).getAttribute("data-id")))
			{
				return true;
			}
		}

		return false;
	}

	private static void removeClass (@Nonnull Element element, @Nonnull String remove) {
		final var className = element.getAttribute("class");
		if (className == null) {
			return;
		}

		final var classes = className.split(" ");
		final var newClasses = new ArrayList<String>();

		for (var cls : classes) {
			if (!cls.equals(remove)) {
				newClasses.add(cls);
			}
		}

		element.setAttribute("class", String.join(" ", newClasses));
	}

	private static void writeAggregate (
		@Nonnull Document document,
		@Nonnull Element container,
		@Nonnull TObjectIntMap<String> aggregate)
	{
		final var list = document.createElement("ul");
		container.appendChild(list);
		aggregate.forEachEntry((item, amount) -> {
			final var listItem = document.createElement("li");
			final var link = document.createElement("a");
			final var amountNode = document.createTextNode(format("%dx ", amount));
			link.setAttribute("href", "#" + URLEncoder.encode(item, UTF_8));
			link.setTextContent(item);
			listItem.appendChild(amountNode);
			listItem.appendChild(link);
			list.appendChild(listItem);
			return true;
		});
	}
}
