'use strict';

function addClass (el, cls) {
	const classes = el.className.split(' ');
	if (!classes.includes(cls)) {
		classes.push(cls);
		el.className = classes.join(' ');
	}
}

function debug (line) {
	const el = document.createElement('div');
	const txt = document.createTextNode(line);
	el.appendChild(txt);
	document.getElementById('debugConsole').appendChild(el);
}

function getParentWithClass (element, cls) {
	if (element === null) {
		return null;
	}

	if (hasClass(element, cls)) {
		return element;
	}

	if (element.tagName.toLowerCase() === 'body') {
		// We've gone too far up.
		return null;
	}

	return getParentWithClass(element.parentElement, cls);
}

function hasClass (el, cls) {
	return el.className.split(' ').includes(cls);
}

function removeClass (el, cls) {
	const classes = el.className.split(' ');
	const clsIndex = classes.indexOf(cls);
	if (clsIndex > -1) {
		classes.splice(clsIndex, 1);
		el.className = classes.join(' ');
	}
}

function selectInfoBox (event) {
	const target = getParentWithClass(event.target, 'info-box');
	const selected = document.getElementsByClassName('selected');

	if (event.ctrlKey) {
		if (hasClass(target, 'selected')) {
			removeClass(target, 'selected');
		} else {
			addClass(target, 'selected');
		}
	} else {
		let otherSelected = 0;
		Array.from(selected).filter(selected => selected !== target).forEach(selected => {
			otherSelected++;
			removeClass(selected, 'selected');
		});

		if (otherSelected > 0 || !hasClass(target, 'selected')) {
			addClass(target, 'selected');
		} else if (hasClass(target, 'selected')) {
			removeClass(target, 'selected');
		}
	}
}

// noinspection JSUnusedGlobalSymbols
function setDebugMode (debugMode) {
	if (debugMode) {
		document.getElementById('debugConsole').style.display = 'block';
	}
}

const pckry = new Packery('body', {itemSelector: '.info-box', gutter: 10});
pckry.bindDraggabillyEvents(new Draggabilly('#master-craft-list', {containment: true}));

const observer =
	new MutationObserver(mutations =>
		mutations.filter(mutation => mutation.type === 'childList').forEach(mutation =>
			mutation.addedNodes.forEach(added => {
				if (added.className === 'info-box') {
					added.addEventListener('click', selectInfoBox, true);
					pckry.appended(added);
					pckry.layout();
					pckry.bindDraggabillyEvents(new Draggabilly(added, {containment: true}));
				}
			})));

observer.observe(document.getElementsByTagName('body')[0], {childList: true});
window.addEventListener('error', err => {
	debug(`[${err.lineno}:${err.colno}] ${err.message}`);
}, true);
