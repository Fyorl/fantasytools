CREATE TABLE Items (
    item_id INTEGER PRIMARY KEY,
    name TEXT NOT NULL UNIQUE,
    type TEXT NOT NULL,
    sellers TEXT NULL,
    dropped_by TEXT NULL
);

CREATE INDEX item_by_name ON Items (name);

CREATE TABLE Recipes (
	recipe_id INTEGER PRIMARY KEY,
	item_fk INTEGER NOT NULL REFERENCES Items (item_id),
	profession TEXT NOT NULL,
	recipe TEXT NOT NULL
);

CREATE TABLE Locations (
	location_id INTEGER PRIMARY KEY,
	item_fk INTEGER NOT NULL REFERENCES Items (item_id),
	profession TEXT NOT NULL,
	location TEXT NOT NULL
);

CREATE TABLE Visited (
    url TEXT NOT NULL UNIQUE
);
