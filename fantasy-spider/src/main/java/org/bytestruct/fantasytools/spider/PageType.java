package org.bytestruct.fantasytools.spider;

enum PageType {
	UNKNOWN,
	PROFESSION,
	RECIPE,
	ITEM,
	GATHERING
}
