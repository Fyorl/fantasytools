package org.bytestruct.fantasytools.spider;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

class Response {
	final int statusCode;
	@Nonnull final String url;
	@Nullable final String contents;

	Response (int statusCode, @Nonnull String url, @Nullable String contents) {
		this.statusCode = statusCode;
		this.url = url;
		this.contents = contents;
	}
}
