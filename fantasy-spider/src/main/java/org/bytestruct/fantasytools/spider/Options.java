package org.bytestruct.fantasytools.spider;

import se.softhouse.jargo.Argument;
import se.softhouse.jargo.Arguments;

import javax.annotation.Nonnull;
import java.io.File;
import java.util.function.Predicate;

class Options {
	private static final int DEFAULT_DELAY = 500;
	private static final int DEFAULT_TIMEOUT = 5000;
	private static final File DEFAULT_DB = new File("spider.db");

	static final Argument<?> help = Arguments.helpArgument("-h", "--help");

	static final Argument<Integer> delay =
		Arguments.integerArgument("--delay")
			.defaultValue(DEFAULT_DELAY)
			.limitTo(positiveInteger())
			.description("The number of ms between each server request to avoid spamming/DoS.")
			.build();

	static final Argument<Integer> timeout =
		Arguments.integerArgument("--timeout")
			.defaultValue(DEFAULT_TIMEOUT)
			.limitTo(positiveInteger())
			.description("HTTP request timeout.")
			.build();

	static final Argument<File> db =
		Arguments.fileArgument("--db")
			.defaultValue(DEFAULT_DB)
			.description("The location of the database to store the scraped data.")
			.build();

	static final Argument<Integer> maxRequests =
		Arguments.integerArgument("--max-requests")
			.limitTo(positiveInteger())
			.description("Perform up to these many requests then exit.")
			.build();

	@Nonnull
	private static Predicate<Integer> positiveInteger () {
		return x -> x >= 0;
	}
}
