package org.bytestruct.fantasytools.spider;

import org.bytestruct.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.annotation.Nonnull;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.OptionalLong;
import java.util.TreeMap;
import java.util.concurrent.BlockingQueue;
import java.util.regex.Pattern;

import static org.bytestruct.fantasytools.data.Data.fetch;

class Job implements Runnable {
	@Nonnull private static final Logger logger = Logger.getLogger(Job.class);
	@Nonnull private static final Pattern nextPattern =
		Pattern.compile("<li class=\"next\"><a href=\"([^\"]+)\"");
	@Nonnull private static final Pattern rowPattern =
		Pattern.compile(
			"<a href=\"([^\"]+)\" class=\"db_popup db-table__txt--detail_link\">([^<]+)</a>");
	@Nonnull private static final Pattern recipeHeaderPattern =
		Pattern.compile(
			"__job_name\">([^<]+)</p>\\s+<div[^>]+>\\s+<p[^>]+>\\s+Lv\\. <span[^>]+>\\d+</span>"
			+ "\\s+(<span[^>]+></span>)*\\s+</p>\\s+</div>\\s+</div>\\s+(<p[^>]+>[^<]+</p>\\s+)?"
			+ "<h2[^>]+>([^<]+)</h2>\\s+<p[^>]+>([^<]+)</p>",
			Pattern.UNICODE_CHARACTER_CLASS);
	@Nonnull private static final Pattern recipePattern =
		Pattern.compile(
			"<span class=\"db-view__item_num\">(\\d+)</span>\\s+<div[^>]+>\\s+"
			+ "<a href=\"([^\"]+)\" class=\"db_popup\"><strong>([^<]+)</strong></a>",
			Pattern.UNICODE_CHARACTER_CLASS);
	@Nonnull private static final Pattern itemDetailPattern =
		Pattern.compile(
			"<a href=\"([^\"]+)\"><img src=\"[^\"]+\" width=\"\\d+\" height=\"\\d+\" "
			+ "alt=\"View Item Details\"></a>");
	@Nonnull private static final Pattern itemHeaderPattern =
		Pattern.compile(
			"<h2 class=\"d[^\"]+\">([^<]+)(<img[^>]+>\\s+)?</h2>\\s+<p[^>]+>([^<]+)</p>",
			Pattern.UNICODE_CHARACTER_CLASS);
	@Nonnull private static final Pattern sellerPattern =
		Pattern.compile("<td[^>]+><a[^>]+>([^<]+)</a></td><td[^>]+>([^<]+)</td>");
	@Nonnull private static final Pattern monsterPattern =
		Pattern.compile(
			"<td[^>]+>\\s+<a[^>]+>([^<]+)</a>\\s+</td>\\s+<td[^>]+>([^<]+)</td>",
			Pattern.UNICODE_CHARACTER_CLASS);
	@Nonnull private static final Pattern gatheringPattern =
		Pattern.compile("<a href=\"(/lodestone/playguide/db/gathering/[^\"]+)\"[^>]+>([^<]+)</a>");
	@Nonnull private static final Pattern craftingPattern =
		Pattern.compile("<a href=\"(/lodestone/playguide/db/recipe/[^\"]+)\"[^>]+>([^<]+)</a>");
	@Nonnull private static final Pattern countryPattern =
		Pattern.compile("<h5 class=\"d[^>]+>([^<]+)</h5>");
	@Nonnull private static final Pattern regionPattern = Pattern.compile("<dt>([^<]+)</dt>");
	@Nonnull private static final Pattern locationPattern = Pattern.compile("<dd>([^<]+)</dd>");

	@Nonnull private final Connection conn;
	@Nonnull private final Response res;
	@Nonnull private final BlockingQueue<String> requestQueue;

	Job (
		@Nonnull Connection conn,
		@Nonnull Response res,
		@Nonnull BlockingQueue<String> requestQueue)
	{
		this.conn = conn;
		this.res = res;
		this.requestQueue = requestQueue;
	}

	private void handleGathering () {
		assert res.contents != null;
		final var elements = new ArrayList<String>();
		final var headerMatcher = recipeHeaderPattern.matcher(res.contents);
		final var countryMatcher = countryPattern.matcher(res.contents);
		final var regionMatcher = regionPattern.matcher(res.contents);
		final var locationMatcher = locationPattern.matcher(res.contents);
		final var countryPositions = new TreeMap<Integer, String>();
		final var regionPositions = new TreeMap<Integer, String>();
		final var locationPositions = new TreeMap<Integer, String>();
		final var locationData = new JSONObject();
		final String profession;
		final OptionalLong itemID;

		if (headerMatcher.find()) {
			final var name =
				headerMatcher.group(4).trim().replace("&#39;", "'").replace("&amp;", "&");
			final var type = headerMatcher.group(5).trim().replace("&#39;", "'");
			elements.add(name);
			profession = headerMatcher.group(1);
			itemID = insertItem(name, type);
		} else {
			logger.warn("Failed to find gathering data header, skipping.");
			return;
		}

		if (itemID.isEmpty()) {
			logger.error("Cannot proceed without item ID, skipping.");
			return;
		}

		while (countryMatcher.find()) {
			countryPositions.put(countryMatcher.end(), countryMatcher.group(1));
		}

		while (regionMatcher.find()) {
			regionPositions.put(regionMatcher.end(), regionMatcher.group(1));
		}

		while (locationMatcher.find()) {
			locationPositions.put(
				locationMatcher.end(),
				locationMatcher.group(1).trim().replaceAll("\\s+", " "));
		}

		for (var country : countryPositions.entrySet()) {
			locationData.put(country.getValue(), new JSONObject());
		}

		for (var region : regionPositions.entrySet()) {
			final var country = countryPositions.get(countryPositions.lowerKey(region.getKey()));
			locationData.getJSONObject(country).put(region.getValue(), new JSONArray());
		}

		for (var location : locationPositions.entrySet()) {
			final var country = countryPositions.get(countryPositions.lowerKey(location.getKey()));
			final var region = regionPositions.get(regionPositions.lowerKey(location.getKey()));
			locationData.getJSONObject(country).getJSONArray(region).put(location);
		}

		insertLocation(itemID.getAsLong(), profession, locationData);
		logger.info("Identified elements: %s", String.join(", ", elements));
	}

	private void handleItem () throws InterruptedException {
		assert res.contents != null;
		final var elements = new ArrayList<String>();
		final var headerMatcher = itemHeaderPattern.matcher(res.contents);
		final var sellerMatcher = sellerPattern.matcher(res.contents);
		final var monsterMatcher = monsterPattern.matcher(res.contents);
		final var gatheringMatcher = gatheringPattern.matcher(res.contents);
		final var craftingMatcher = craftingPattern.matcher(res.contents);
		final var sellers = new JSONObject();
		final var monsters = new JSONObject();
		final OptionalLong itemID;

		if (headerMatcher.find()) {
			final var name =
				headerMatcher.group(1).trim().replace("&#39;", "'").replace("&amp;", "&");
			final var type = headerMatcher.group(3).trim().replace("&#39;", "'");
			elements.add(name);
			itemID = insertItem(name, type);
		} else {
			logger.warn("Failed to find item header, skipping.");
			return;
		}

		if (itemID.isEmpty()) {
			logger.error("Cannot proceed without item ID, skipping.");
			return;
		}

		while (sellerMatcher.find()) {
			final var seller =
				sellerMatcher.group(1).trim().replace("&#39;", "'").replace("&amp;", "&");
			final var location = sellerMatcher.group(2).trim();
			elements.add(seller);
			sellers.put(seller, location);
		}

		while (monsterMatcher.find()) {
			final var monster = monsterMatcher.group(1).trim().replace("&#39;", "'");
			final var location = monsterMatcher.group(2).trim().replace(" Other Locations", "");
			elements.add(monster);
			monsters.put(monster, location);
		}

		while (gatheringMatcher.find()) {
			elements.add(gatheringMatcher.group(2).trim());
			requestQueue.put(gatheringMatcher.group(1));
		}

		while (craftingMatcher.find()) {
			final var profession = craftingMatcher.group(2).trim();
			if (profession.contains(profession)) {
				elements.add(profession);
				requestQueue.put(craftingMatcher.group(1));
			}
		}

		updateItem(itemID.getAsLong(), sellers, monsters);
		logger.info("Identified elements: %s", String.join(", ", elements));
	}

	private void handleProfession () throws InterruptedException {
		assert res.contents != null;
		final var elements = new ArrayList<String>();
		final var nextMatcher = nextPattern.matcher(res.contents);
		final var rowMatcher = rowPattern.matcher(res.contents);

		if (nextMatcher.find()) {
			elements.add("NEXT");
			requestQueue.put(nextMatcher.group(1).substring(FantasySpider.domain.length()));
		}

		while (rowMatcher.find()) {
			elements.add(rowMatcher.group(2));
			requestQueue.put(rowMatcher.group(1));
		}

		logger.info("Identified elements: %s", String.join(", ", elements));
	}

	private void handleRecipe () throws InterruptedException {
		assert res.contents != null;
		final var elements = new ArrayList<String>();
		final var recipe = new JSONObject();
		final var headerMatcher = recipeHeaderPattern.matcher(res.contents);
		final var recipeMatcher = recipePattern.matcher(res.contents);
		final var detailsMatcher = itemDetailPattern.matcher(res.contents);
		final String profession;
		final OptionalLong itemID;

		if (headerMatcher.find()) {
			final var name =
				headerMatcher.group(4).trim().replace("&#39;", "'").replace("&amp;", "&");
			final var type = headerMatcher.group(5).trim().replace("&#39;", "'");
			elements.add(name);
			profession = headerMatcher.group(1);
			itemID = insertItem(name, type);
		} else {
			logger.warn("Failed to find recipe header, skipping.");
			return;
		}

		if (itemID.isEmpty()) {
			logger.error("Cannot proceed without item ID, skipping.");
			return;
		}

		if (detailsMatcher.find()) {
			elements.add("DETAILS");
			requestQueue.put(detailsMatcher.group(1));
		}

		while (recipeMatcher.find()) {
			final var amount = recipeMatcher.group(1);
			final var name = recipeMatcher.group(3);
			elements.add(name);
			recipe.put(name, Integer.parseInt(amount));
			requestQueue.put(recipeMatcher.group(2));
		}

		insertRecipe(itemID.getAsLong(), profession, recipe);
		logger.info("Identified elements: %s", String.join(", ", elements));
	}

	@Nonnull
	private OptionalLong insertItem (@Nonnull String name, @Nonnull String type) {
		var itemID = OptionalLong.empty();
		try (var stmt = conn.prepareStatement("SELECT * FROM Items WHERE name = ?")) {
			stmt.setString(1, name);
			try (var rs = stmt.executeQuery()) {
				final var rows = fetch(rs);
				if (rows.size() > 0) {
					itemID = OptionalLong.of(rows.get(0).get("item_id").getAsLong());
				}
			}
		} catch (SQLException e) {
			logger.error("Query failed when checking for existing item '%s'.", e, name);
		}

		if (itemID.isEmpty()) {
			try (var stmt =	conn.prepareStatement("INSERT INTO Items (name, type) VALUES (?, ?)")) {
				stmt.setString(1, name);
				stmt.setString(2, type);
				stmt.executeUpdate();

				try (var rs = stmt.getGeneratedKeys()) {
					if (rs.next()) {
						itemID = OptionalLong.of(rs.getLong(1));
					}
				}
			} catch (SQLException e) {
				logger.error("Failed to insert item '%s'.", e, name);
			}
		}

		return itemID;
	}

	private void insertLocation (
		long itemID,
		@Nonnull String profession,
		@Nonnull JSONObject location)
	{
		try (var stmt =
				conn.prepareStatement(
					"INSERT INTO Locations (item_fk, profession, location) VALUES (?, ?, ?)"))
		{
			stmt.setLong(1, itemID);
			stmt.setString(2, profession);
			stmt.setString(3, location.toString());
			stmt.executeUpdate();
		} catch (SQLException e) {
			logger.error("Failed to insert location.", e);
		}
	}

	private void insertRecipe (
		long itemID,
		@Nonnull String profession,
		@Nonnull JSONObject recipe)
	{
		try (var stmt =
				conn.prepareStatement(
					"INSERT INTO Recipes (item_fk, profession, recipe) VALUES (?, ?, ?)"))
		{
			stmt.setLong(1, itemID);
			stmt.setString(2, profession);
			stmt.setString(3, recipe.toString());
			stmt.executeUpdate();
		} catch (SQLException e) {
			logger.error("Failed to insert recipe.", e);
		}
	}

	private void insertVisited (@Nonnull String url) {
		try (var stmt = conn.prepareStatement("INSERT INTO Visited (url) VALUES (?)")) {
			stmt.setString(1, url);
			stmt.executeUpdate();
		} catch (SQLException e) {
			logger.error("Failed to insert visited URL.", e);
		}
	}

	private void updateItem (
		long itemID,
		@Nonnull JSONObject sellers,
		@Nonnull JSONObject monsters)
	{
		try (var stmt =
				conn.prepareStatement(
					"UPDATE Items SET sellers = ?, dropped_by = ? WHERE item_id = ?"))
		{
			stmt.setString(1, sellers.toString());
			stmt.setString(2, monsters.toString());
			stmt.setLong(3, itemID);
			stmt.executeUpdate();
		} catch (SQLException e) {
			logger.error("Failed to update item %d.", e, itemID);
		}
	}

	@Override
	public void run () {
		try {
			final var pageType = FantasySpider.determinePageType(res.url);
			logger.info("Determined page type '%s'.", pageType);

			if (pageType != PageType.PROFESSION) {
				insertVisited(res.url);
			}

			switch (pageType) {
				case PROFESSION:
					handleProfession();
					break;

				case RECIPE:
					handleRecipe();
					break;

				case ITEM:
					handleItem();
					break;

				case GATHERING:
					handleGathering();
					break;
			}
		} catch (InterruptedException e) {
			logger.error("Job thread interrupted, exiting.");
		}
	}
}
