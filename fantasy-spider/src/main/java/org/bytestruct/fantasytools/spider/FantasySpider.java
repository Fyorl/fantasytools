package org.bytestruct.fantasytools.spider;

import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.HttpClientBuilder;
import org.bytestruct.logging.Logger;
import se.softhouse.jargo.ArgumentException;
import se.softhouse.jargo.CommandLineParser;
import se.softhouse.jargo.ParsedArguments;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.*;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static org.bytestruct.fantasytools.data.Data.fetch;

public class FantasySpider {
	@Nonnull private static final Logger logger = Logger.getLogger(FantasySpider.class);
	@Nonnull private static final String root = "/lodestone/playguide/db/recipe/";
	@Nonnull static final String domain = "https://eu.finalfantasyxiv.com";

	public static void main (@Nonnull String[] args) {
		try {
			final var opts =
				CommandLineParser
					.withArguments(Options.delay, Options.timeout, Options.db, Options.maxRequests)
					.andArguments(Options.help)
					.parse(args);

			final var spider = new FantasySpider(opts);
			spider.spider();
		} catch (ArgumentException e) {
			System.out.printf("%s%n", e.getMessageAndUsage());
			System.exit(1);
		}
	}

	@Nonnull
	private static Set<String> loadVisited (@Nonnull Connection conn) {
		try (var stmt = conn.createStatement();
			 var rs = stmt.executeQuery("SELECT * FROM Visited"))
		{
			final var rows = fetch(rs);
			return rows.stream().map(row -> row.get("url").get()).collect(Collectors.toSet());
		} catch (SQLException e) {
			logger.warn("Failed to load list of visited URLs.", e);
		}

		return Collections.emptySet();
	}

	@Nonnull
	private static String readAll (@Nonnull InputStream stream, @Nonnull Charset charset)
		throws IOException
	{
		try (var in = new InputStreamReader(stream, charset)) {
			final var contents = new StringBuilder();
			final var buf = new char[8192];
			int n;

			while ((n = in.read(buf)) > -1) {
				contents.append(buf, 0, n);
			}

			return contents.toString();
		}
	}

	@Nonnull
	static PageType determinePageType (@Nonnull String url) {
		final var path = url.split("/");
		if ((path.length > 7 && path[6].equals("recipe") && path[7].startsWith("?"))
			|| path.length < 8)
		{
			return PageType.PROFESSION;
		}

		if (path[6].equals("recipe")) {
			return PageType.RECIPE;
		}

		if (path[6].equals("item")) {
			return PageType.ITEM;
		}

		if (path[6].equals("gathering")) {
			return PageType.GATHERING;
		}

		return PageType.UNKNOWN;
	}

	private final int delay;
	private final int timeout;
	private final int maxRequests;
	@Nonnull private final File db;
	@Nonnull private final HttpClient client;
	@Nonnull private final BlockingQueue<String> requestQueue = new LinkedBlockingQueue<>();
	@Nonnull private final ExecutorService consumer = Executors.newSingleThreadExecutor();

	private FantasySpider (@Nonnull ParsedArguments opts) {
		final var delay = opts.get(Options.delay);
		final var timeout = opts.get(Options.timeout);
		final var db = opts.get(Options.db);
		final var maxRequests = opts.get(Options.maxRequests);

		assert delay != null;
		assert timeout != null;
		assert db != null;
		assert maxRequests != null;

		this.delay = delay;
		this.timeout = timeout;
		this.db = db;
		this.maxRequests = opts.wasGiven(Options.maxRequests) ? maxRequests : Integer.MAX_VALUE;

		client = buildClient();
	}

	@Nonnull
	private HttpClient buildClient () {
		final var config =
			RequestConfig.custom()
				.setConnectionRequestTimeout(timeout)
				.setCookieSpec(CookieSpecs.STANDARD)
				.build();

		return HttpClientBuilder.create().setDefaultRequestConfig(config).build();
	}

	private void initialiseDB (@Nonnull Connection conn) throws IOException, SQLException {
		logger.info("Initialising DB.");
		final var ddl =
			readAll(getClass().getResourceAsStream("/master.sql"), StandardCharsets.UTF_8);

		try (var stmt = conn.createStatement()) {
			stmt.executeUpdate(ddl);
		}
	}

	@Nonnull
	private String jdbcURL () {
		return format("jdbc:sqlite:%s", db.getAbsolutePath());
	}

	@Nonnull
	private Response performRequest (@Nonnull String url) {
		logger.info("HTTP GET %s", url);
		final var get = new HttpGet(url);

		try {
			final var res = client.execute(get);
			final var entity = res.getEntity();
			final var contentType = ContentType.getOrDefault(entity);
			final var statusCode = res.getStatusLine().getStatusCode();

			if (statusCode == 200) {
				return new Response(
					statusCode,
					url,
					readAll(entity.getContent(), contentType.getCharset()));
			} else {
				return new Response(statusCode, url, null);
			}
		} catch (IOException e) {
			logger.error("GET request to '%s' failed.", e, url);
		} finally {
			get.releaseConnection();
		}

		return new Response(-1, url, null);
	}

	private void shutdownConsumer () {
		consumer.shutdown();
		try {
			if (consumer.awaitTermination(10, TimeUnit.SECONDS)) {
				consumer.shutdownNow();
				if (!consumer.awaitTermination(10, TimeUnit.SECONDS)) {
					logger.error("Failed to terminate consumer.");
				}
			}
		} catch (InterruptedException e) {
			consumer.shutdownNow();
		}
	}

	private void spider () {
		logger.info("Fantasy Spider starting.");
		try (var conn = DriverManager.getConnection(jdbcURL())) {
			if (uninitialisedDB(conn)) {
				initialiseDB(conn);
			}

			startAtRoot(conn, loadVisited(conn));
		} catch (SQLException e) {
			logger.error("Failed to connect to database.", e);
		} catch (IOException e) {
			logger.error("Failed to read master.sql.", e);
		}
	}

	private void startAtRoot (@Nonnull Connection conn, @Nonnull Set<String> visitedURLs) {
		int count = 0;
		try {
			requestQueue.put(root);
			String path;

			while ((path = requestQueue.poll()) != null && count < maxRequests) {
				final var url = domain + path;
				final var pageType = determinePageType(url);

				if (visitedURLs.contains(url)) {
					logger.info("Already seen URL '%s', skipping.", url);
					continue;
				}

				if (pageType != PageType.PROFESSION) {
					visitedURLs.add(url);
				}

				final var res = performRequest(url);
				if (res.statusCode == HttpStatus.SC_NOT_FOUND) {
					logger.warn("URL '%s' not found, skipping.", url);
					continue;
				}

				if (res.statusCode == HttpStatus.SC_OK && res.contents != null) {
					consumer.execute(new Job(conn, res, requestQueue));
				} else {
					logger.info("Failed to retrieve URL '%s', retrying.", url);
					requestQueue.put(path);
				}

				Thread.sleep(delay);
				count++;
			}
		} catch (InterruptedException e) {
			logger.error("Main thread interrupted, exiting.");
			shutdownConsumer();
		}

		shutdownConsumer();
	}

	private boolean uninitialisedDB (@Nonnull Connection conn) {
		logger.info("Checking DB is initialised.");
		try (var stmt = conn.createStatement()) {
			stmt.executeQuery("SELECT COUNT(*) FROM Items").close();
		} catch (SQLException e) {
			return true;
		}

		return false;
	}
}
