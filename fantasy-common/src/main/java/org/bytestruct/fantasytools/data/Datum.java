package org.bytestruct.fantasytools.data;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class Datum {
	@Nonnull private final String datum;

	Datum (@Nonnull String datum) {
		this.datum = datum;
	}

	@Nullable
	public String get () {
		return datum;
	}

	public long getAsLong () {
		return Long.parseLong(datum);
	}

	public int getAsInt () {
		return Integer.parseInt(datum);
	}

	public double getAsDouble () {
		return Double.parseDouble(datum);
	}

	public boolean getAsBoolean () {
		return datum.equals("1");
	}
}
