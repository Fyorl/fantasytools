package org.bytestruct.fantasytools.data;

import javax.annotation.Nonnull;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Data {
	@Nonnull
	public static List<Map<String, Datum>> fetch (@Nonnull ResultSet rs) throws SQLException {
		final var results = new ArrayList<Map<String, Datum>>();
		final var columnNames = new ArrayList<String>();
		final var metadata = rs.getMetaData();
		final var columnCount = metadata.getColumnCount();

		for (var i = 1; i <= columnCount; i++) {
			columnNames.add(metadata.getColumnName(i));
		}

		while (rs.next()) {
			final var row = new HashMap<String, Datum>();
			results.add(row);

			for (var i = 1; i <= columnCount; i++) {
				row.put(columnNames.get(i - 1), new Datum(rs.getString(i)));
			}
		}

		return results;
	}
}
