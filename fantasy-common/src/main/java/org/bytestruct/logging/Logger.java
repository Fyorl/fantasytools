package org.bytestruct.logging;

import org.apache.logging.log4j.LogManager;

import javax.annotation.Nonnull;

import static java.lang.String.format;

public class Logger {
	@Nonnull
	public static Logger getLogger (@Nonnull Class<?> cls) {
		return new Logger(LogManager.getLogger(cls));
	}

	private final org.apache.logging.log4j.Logger delegate;

	private Logger (@Nonnull org.apache.logging.log4j.Logger delegate) {
		this.delegate = delegate;
	}

	public void info (@Nonnull String msg, @Nonnull Object... args) {
		delegate.info(format(msg, args));
	}

	public void info (@Nonnull String msg, @Nonnull Throwable e, @Nonnull Object... args) {
		delegate.info(format(msg, args), e);
	}

	public void debug (@Nonnull String msg, @Nonnull Object... args) {
		delegate.debug(format(msg, args));
	}

	public void debug (@Nonnull String msg, @Nonnull Throwable e, @Nonnull Object... args) {
		delegate.debug(format(msg, args), e);
	}

	public void warn (@Nonnull String msg, @Nonnull Object... args) {
		delegate.warn(format(msg, args));
	}

	public void warn (@Nonnull String msg, @Nonnull Throwable e, @Nonnull Object... args) {
		delegate.warn(format(msg, args), e);
	}

	public void error (@Nonnull String msg, @Nonnull Object... args) {
		delegate.error(format(msg, args));
	}

	public void error (@Nonnull String msg, @Nonnull Throwable e, @Nonnull Object... args) {
		delegate.error(format(msg, args), e);
	}

	public void trace (@Nonnull String msg, @Nonnull Object... args) {
		delegate.trace(format(msg, args));
	}

	public void trace (@Nonnull String msg, @Nonnull Throwable e, @Nonnull Object... args) {
		delegate.trace(format(msg, args), e);
	}
}
