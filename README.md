# Fantasy Tools
FFXIV quick reference tools. GPL3.

## Requirements
* Java 11+
* Maven 3.6+

## Building
```
$ mvn clean install
```

## Running
### fantasy-spider
The spider crawls the online FFXIV database to scrape item information. It stores which NPCs sell the item, which monsters drop it, what crafting recipes can create the item, and the locations the item can be gathered. 

This information is stored in a local SQLite database to allow the `fantasy-app` tool to reference it without performing any web requests.

After building, the spider can be run with:
```
$ java -jar fantasy-spider/target/fantasy-spider.jar
```

It has the following command-line options:

* `--db <path>` The location of the SQLite database, any valid filename should work. The file does not have to already exist. By default this creates a file called `spider.db` in the working directory.
* `--delay <integer>` The number of milliseconds between each server request. Please be responsible and do not spam the server with requests. I accept no responsibility for any consequences incurred as a result of misusing this tool. The default is 500ms and it takes approximately 9 hours to crawl the entire FFXIV item database at that speed.
* `--timeout <integer>` HTTP request timeout in milliseconds. The default is 5,000ms.

### fantasy-app
The app is a small tool used to visualise the data downloaded by the spider. 

After building, it can be run with:
```
$ java -jar fantasy-app/target/fantasy-app.jar
```

It has the following command-line options:

* `--db <path>` The location of the SQLite database containing the spidered content.

The app consists of an item search on the left and a workspace on the right. After searching for an item, it can be double-clicked from the list and added to the workspace. Items in the workspace can be selected by clicking (multi-select by holding ctrl) and rearranged by dragging. To delete all selected items, press 'delete'. To add all selected items to the craft list, press 'enter'.

Adding an item to the craft list will show all the ingredients required to craft it. If any of the ingredients also have recipes, they will be shown too, and so on.

At the end, all of the base ingredients will be totalled up. A base ingredient is one that does not have any recipes associated with it and must instead be bought or crafted. You may click any of the items in the master craft list to add that item into the workspace and see where it is sold or how it may be gathered.
